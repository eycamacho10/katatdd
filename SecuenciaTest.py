# coding=utf-8

from unittest import TestCase

from Secuencia import Secuencia


class SecuenciaTest(TestCase):
    def test_obtenerEstadisticas_cadena_vacia(self):
        self.assertEquals(Secuencia().obtenerEstadisticas(""),[0, 0, 0, 0],"Cadena Vacía")

    def test_obtenerEstadisticas_unnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9"), [1, 9, 9, 9], "Un numero")

    def test_obtenerEstadisticas_dosnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8"), [2, 8, 9, 8.5], "Dos numeros")

    def test_obtenerEstadisticas_multiplesnumeros(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8,7,6,4"), [5, 4, 9, 6.8], "Multiples numeros")

    def test_obtenerEstadisticas_con_minimo_cadena_vacia(self):
        self.assertEquals(Secuencia().obtenerEstadisticas(""),[0, 0, 0, 0],"Cadena Vacía")

    def test_obtenerEstadisticas_con_minimo_unnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9"), [1, 9, 9, 9], "Un numero")

    def test_obtenerEstadisticas_con_minimo_dosnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8"), [2, 8, 9, 8.5], "Dos numeros")

    def test_obtenerEstadisticas_con_minimo_multiplesnumeros(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8,7,6,4"), [5, 4, 9, 6.8], "Multiples numeros")

    def test_obtenerEstadisticas_con_minimo_con_maximo_cadena_vacia(self):
        self.assertEquals(Secuencia().obtenerEstadisticas(""),[0, 0, 0, 0],"Cadena Vacía")

    def test_obtenerEstadisticas_con_minimo_con_maximo_unnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9"), [1, 9, 9, 9], "Un numero")

    def test_obtenerEstadisticas_con_minimo_con_maximo_dosnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8"), [2, 8, 9, 8.5], "Dos numeros")

    def test_obtenerEstadisticas_con_minimo_con_maximo_multiplesnumeros(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8,7,6,4"), [5, 4, 9, 6.8], "Multiples numeros")

    def test_obtenerEstadisticas_promedio_cadena_vacia(self):
        self.assertEquals(Secuencia().obtenerEstadisticas(""),[0, 0, 0, 0],"Cadena Vacía")

    def test_obtenerEstadisticas_promedio_unnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9"), [1, 9, 9, 9], "Un numero")

    def test_obtenerEstadisticas_promedio_dosnumero(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8"), [2, 8, 9, 8.5], "Dos numeros")

    def test_obtenerEstadisticas_promedio_multiplesnumeros(self):
        self.assertEquals(Secuencia().obtenerEstadisticas("9,8,7,6,4"), [5, 4, 9, 6.8], "Multiples numeros")