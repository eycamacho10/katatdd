class Secuencia:
    def obtenerEstadisticas(self, secuencia):
        if secuencia is None or secuencia == "":
            return [0, 0, 0, 0]
        elif "," in secuencia:
            numeros = secuencia.split(",")
            numeros_int = [int(numero) for numero in numeros]
            return [len(numeros), int(min(numeros)), int(max(secuencia)), sum(numeros_int)/float(len(numeros_int))]
        return [len(secuencia), int(min(secuencia)), int(max(secuencia)), int(secuencia)]